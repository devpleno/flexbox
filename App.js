import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.box1} />

        <View style={styles.box2} />

        <View style={styles.box3} >
          <View style={styles.subbox} />
          <View style={styles.subbox} />
          <View style={styles.subbox} />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  box1: {
    flex: 1,
    backgroundColor: 'blue',
    margin: 4
  },
  box2: {
    flex: 2,
    backgroundColor: 'red',
    margin: 4
  },
  box3: {
    height: 100,
    backgroundColor: 'steelblue',
    margin: 4,
    flexDirection: 'row'
  },
  subbox: {
    flex: 1,
    backgroundColor: 'yellow',
    margin: 4
  }
});
